# Vehicle MVI

This is a sample application which gets a list of object from an API and shows to user 

## Motivation

To demonstrate how to write  a clean, extensible and testable code with MVI architecture

## Architecture

- [MVI Architecture](http://hannesdorfmann.com/android/model-view-intent)

- [Watch youtube video here](https://www.youtube.com/watch?v=PXBXcHQeDLE)

<img src="https://cdn-images-1.medium.com/max/800/1*UHHDfaKEqAaamuklZNCCzA.png" alt="MVI" style="max-width: 100%; height: auto;">

- Model: function from Observable of actions to Observable of state
- View: function from Observable of state to Observable of rendering
- Intent: function from Observable of user events to Observable of “actions”
- 
## Navigation

Navigation handled with simple injectable Navigator class, but for the complex apllication [Coordinator pattern](http://hannesdorfmann.com/android/coordinators-android) should be used

## Technics

- [Rxjava](https://github.com/ReactiveX/RxJava)
- [Room](https://developer.android.com/topic/libraries/architecture/room)
- [Dagger](https://github.com/google/dagger)
- [Retrofit](https://github.com/square/retrofit)

## Testing 
- [Junit](https://junit.org/junit5/)
- [Mockito2](https://github.com/mockito/mockito)

## Features

- [x] Offline support


## License

    Copyright 2018 Talha Kosen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.