package com.talhakosen.vehiclemvi

import android.os.Bundle
import com.talhakosen.vehiclemvi.common.navigation.Navigator
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigator.activity = this
        if (savedInstanceState == null) {
            navigator.addVehicleListFragment()
        }
    }
}
