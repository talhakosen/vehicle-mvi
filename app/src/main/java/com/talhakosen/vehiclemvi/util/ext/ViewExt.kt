package com.talhakosen.vehiclemvi.util.ext

import android.content.Context
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.talhakosen.vehiclemvi.R
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Color

/**
 * Applies [width] & [height] to existing [View.getLayoutParams] and performs [View.setLayoutParams].
 *
 * If [View.getLayoutParams] == null, then [ViewGroup.MarginLayoutParams] with [width] & [height] will be created.
 */
fun View.applyLayoutParams(width: Int = MATCH_PARENT, height: Int = WRAP_CONTENT) {
    layoutParams = this.layoutParams?.apply {
        this.width = width
        this.height = height
    } ?: ViewGroup.MarginLayoutParams(width, height)
}

inline fun View.doInRuntime(code: () -> Unit) {
    if (!isInEditMode) code()
}

/**
 * Size conversion involves rounding the base value, and ensuring that a non-zero base value
 * is at least one pixel in size.
 */
fun Context.getDimensionPixelSizeCompat(@DimenRes resId: Int): Int = resources.getDimensionPixelSize(resId)

@ColorInt
fun Context.getColorCompat(@ColorRes colorRes: Int): Int = ContextCompat.getColor(this, colorRes)

@ColorInt
fun View.getColor(@ColorRes resId: Int): Int = context.getColorCompat(resId)

fun View.setColor(colorName: Color) {
    when (colorName) {
        Color.Blue -> setBackgroundColor(getColor(R.color.Blue))
        Color.White -> setBackgroundColor(getColor(R.color.White))
        Color.Green -> setBackgroundColor(getColor(R.color.Green))
        Color.Yellow -> setBackgroundColor(getColor(R.color.Yellow))
        Color.Brown -> setBackgroundColor(getColor(R.color.Brown))
        Color.Red -> setBackgroundColor(getColor(R.color.Red))
        Color.Black -> setBackgroundColor(getColor(R.color.Black))
        Color.Unknown -> setBackgroundColor(getColor(R.color.White))
    }
}

// region Visibility
///////////////////////////////////////////////////////////////////////////

fun View.isVisible() = visibility == View.VISIBLE
fun View.setVisible() {
    if (!isVisible())
        visibility = View.VISIBLE
}

fun View.isInvisible() = visibility == View.INVISIBLE
fun View.setInvisible() {
    if (!isInvisible())
        visibility = View.INVISIBLE
}

fun View.isGone() = visibility == View.GONE
fun View.setGone() {
    if (!isGone())
        visibility = View.GONE
}

// endregion
