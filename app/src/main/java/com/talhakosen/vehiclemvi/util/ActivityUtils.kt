package com.talhakosen.vehiclemvi.util

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.talhakosen.vehiclemvi.R
import timber.log.Timber

object ActivityUtils {

    fun addFragment(activity: AppCompatActivity?, fragment: Fragment) {
        if (activity != null) {
            activity.supportFragmentManager
                .beginTransaction()
                .add(R.id.container, fragment, "")
                .commit()
        } else {
            Timber.d("Activity has not been set for " + fragment::class.java.simpleName)
        }
    }

    fun replaceFragmentToBackStage(activity: AppCompatActivity?, fragment: Fragment, tag: String) {
        if (activity != null) {
            activity.supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit()
        } else {
            Timber.d("Activity has not been set for " + fragment::class.java.simpleName)
        }
    }
}
