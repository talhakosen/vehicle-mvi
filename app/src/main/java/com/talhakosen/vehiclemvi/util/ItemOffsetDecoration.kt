package com.talhakosen.vehiclemvi.util

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class ItemOffsetDecoration(
    private val leftOffset: Int = 0,
    private val topOffset: Int = 0,
    private val rightOffset: Int = 0,
    private val bottomOffset: Int = 0
) : RecyclerView.ItemDecoration() {

    constructor(offset: Int) : this(offset, offset, offset, offset)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = leftOffset
        outRect.right = rightOffset
        outRect.bottom = bottomOffset
        outRect.top = topOffset
    }
}
