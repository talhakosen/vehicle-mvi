package com.talhakosen.vehiclemvi.util.ext

import android.support.annotation.LayoutRes
import android.view.View
import android.view.ViewGroup

// region Helpers
///////////////////////////////////////////////////////////////////////////

fun ViewGroup.selfInflate(@LayoutRes resId: Int) {
    View.inflate(context, resId, this)
}

// endregion