package com.talhakosen.vehiclemvi

import android.content.Context
import android.support.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.talhakosen.vehiclemvi.BuildConfig.DEBUG_ENABLED
import com.talhakosen.vehiclemvi.common.di.DaggerAppComponent
import com.talhakosen.vehiclemvi.common.di.NetworkModuleConfig
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

class BaseApplication : DaggerApplication() {
    companion object {
        private const val BASE_URL = "http://private-6d86b9-vehicles5.apiary-mock.com/"
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        if (DEBUG_ENABLED) {
            Stetho.initializeWithDefaults(this)
            Timber.plant(Timber.DebugTree())
        }

        // TODO Remote config can be used to get this url
        NetworkModuleConfig.init(BASE_URL)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
