package com.talhakosen.vehiclemvi.common.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.talhakosen.vehiclemvi.common.database.entities.VehicleEntity

@Database(
    entities = [
        VehicleEntity::class
    ],
    exportSchema = false,
    version = VehicleDatabase.DB_VERSION
)

abstract class VehicleDatabase : RoomDatabase() {

    companion object {
        const val DB_VERSION = 1
        const val DB_NAME = "vehicle-db"
    }

    abstract fun vehicleDao(): VehicleDao
}
