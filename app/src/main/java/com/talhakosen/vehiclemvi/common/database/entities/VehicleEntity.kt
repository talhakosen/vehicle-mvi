package com.talhakosen.vehiclemvi.common.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


// region M4 Preview Entity
///////////////////////////////////////////////////////////////////////////

@Entity(
    tableName = VehicleEntity.TABLE_NAME
)
data class VehicleEntity(
    @PrimaryKey
    @ColumnInfo(name = VEHICLE_ID)
    var vehicleId: Long = 0,

    @ColumnInfo(name = VRN)
    var vrn: String = "",

    @ColumnInfo(name = COUNTRY)
    var country: String = "",

    @ColumnInfo(name = COLOR)
    var color: String = "",

    @ColumnInfo(name = TYPE)
    var type: String = "",

    @ColumnInfo(name = DEFAULT)
    var default: Boolean = false
) {
    companion object {
        const val TABLE_NAME = "vehicles"

        const val VEHICLE_ID = "vehicle_id"
        const val VRN = "vehicle_name"
        const val COUNTRY = "vehicle_country"
        const val COLOR = "vehicle_color"
        const val TYPE = "vehicle_type"
        const val DEFAULT = "default"
    }
}

// endregion
