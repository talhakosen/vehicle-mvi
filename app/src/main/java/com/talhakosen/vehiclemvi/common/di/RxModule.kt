package com.talhakosen.vehiclemvi.common.di

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import javax.inject.Qualifier

// region ReactiveX Qualifiers

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class IoScheduler

// endregion

@Module
object RxModule {

    /**
     * Provides a IO Scheduler
     * @return a Scheduler
     */
    @JvmStatic
    @Provides
    @IoScheduler
    fun provideIoScheduler() = Schedulers.io()
}
