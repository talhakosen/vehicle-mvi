package com.talhakosen.vehiclemvi.common.di

import com.talhakosen.vehiclemvi.common.navigation.Navigator
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.Scheduler
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
object CommonModule {

    /**
     * Provides the Navigator,
     * @return a singleton Navigator instance
     */
    @Singleton
    @JvmStatic
    @Provides
    fun navigator() = Navigator()

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(@IoScheduler scheduler: Scheduler): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NetworkModuleConfig.baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(scheduler))
            .build()
    }
}
