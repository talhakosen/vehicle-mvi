package com.talhakosen.vehiclemvi.common

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

/**
 * An injectable NetworkManager which allow the check network status
 */
class NetworkManager @Inject constructor(
    private var context: Context
) {
    fun isNetworkAvailable(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
    }
}
