package com.talhakosen.vehiclemvi.common.database.di

import android.arch.persistence.room.Room
import android.content.Context
import com.talhakosen.vehiclemvi.common.database.VehicleDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [VehicleModule::class]
)
class DatabaseModule {
    /**
     * Provides a singleton database
     * @param context the Context used to instantiate the repository
     * @return Context
     */
    @Singleton
    @Provides
    fun provideVeonDatabase(
        context: Context
    ): VehicleDatabase = Room.databaseBuilder(
        context,
        VehicleDatabase::class.java,
        VehicleDatabase.DB_NAME
    ).build()
}

@Module
class VehicleModule {

    /**
     * Provides a singleton VehicleDao implementation
     * @param vehicleDatabase the VehicleDatabase used to instantiate the VehicleDao
     * @return Context
     */
    @Singleton
    @Provides
    fun provideVehicleDao(vehicleDatabase: VehicleDatabase) = vehicleDatabase.vehicleDao()
}
