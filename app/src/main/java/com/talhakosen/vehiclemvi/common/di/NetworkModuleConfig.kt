package com.talhakosen.vehiclemvi.common.di

/**
 * Allow to set base url dynamically
 */
object NetworkModuleConfig {

    lateinit var baseUrl: String
        private set

    @JvmStatic
    fun init(baseUrl: String) {
        NetworkModuleConfig.baseUrl = baseUrl
    }
}