package com.talhakosen.vehiclemvi.common.navigation

import android.support.v7.app.AppCompatActivity
import com.talhakosen.vehiclemvi.ui.vehicledetail.VehicleDetailFragment
import com.talhakosen.vehiclemvi.ui.vehiclelist.VehicleListFragment
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.util.ActivityUtils.addFragment
import com.talhakosen.vehiclemvi.util.ActivityUtils.replaceFragmentToBackStage
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Simple Navigator
 * Navigation handled with simple injectable Navigator class, but for the complex apllication Coordinator pattern should be used
 */
@Singleton
class Navigator @Inject constructor() {
    var activity: AppCompatActivity? = null

    fun addVehicleListFragment() {
        if (activity == null)
            throw (Throwable("Activity should be implemented to Navigator "))

        addFragment(
            activity = activity,
            fragment = VehicleListFragment()
        )
    }

    fun addVehicleDetailFragment(vehicle: Vehicle) {
        if (activity == null)
            throw (Throwable("Activity should be implemented to Navigator "))

        replaceFragmentToBackStage(
            activity = activity,
            fragment = VehicleDetailFragment.newInstance(vehicle),
            tag = "VehicleDetailFragment"
        )
    }
}
