package com.talhakosen.vehiclemvi.common.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {
    /**
     * Expose Application as an injectable context
     * @return Context
     */
    @Binds
    internal abstract fun bindContext(application: Application): Context
}
