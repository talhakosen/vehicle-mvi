package com.talhakosen.vehiclemvi.common.di

import com.talhakosen.vehiclemvi.MainActivity
import com.talhakosen.vehiclemvi.ui.vehiclelist.di.VehicleFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [(VehicleFragmentModule::class)])
    internal abstract fun contributeMainActivity(): MainActivity
}
