package com.talhakosen.vehiclemvi.common.di

import android.app.Application
import com.talhakosen.vehiclemvi.BaseApplication
import com.talhakosen.vehiclemvi.common.database.di.DatabaseModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Scope
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ActivityBindingModule::class,
        AndroidSupportInjectionModule::class,
        CommonModule::class,
        RxModule::class,
        DatabaseModule::class
    ]
)

interface AppComponent : AndroidInjector<BaseApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}
