package com.talhakosen.vehiclemvi.common.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.talhakosen.vehiclemvi.common.database.entities.VehicleEntity
import io.reactivex.Flowable


@Dao
abstract class VehicleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vehicleEntities: List<VehicleEntity>)

    @Query("SELECT * FROM ${VehicleEntity.TABLE_NAME}")
    abstract fun getAll(): Flowable<List<VehicleEntity>>
}
