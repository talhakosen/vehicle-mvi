package com.talhakosen.vehiclemvi.ui.vehiclelist.di

import com.talhakosen.vehiclemvi.ui.vehiclelist.VehicleListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class VehicleFragmentModule {
    @ContributesAndroidInjector(modules = [(VehicleListModule::class)])
    abstract fun contributeVehicleModule(): VehicleListFragment
}