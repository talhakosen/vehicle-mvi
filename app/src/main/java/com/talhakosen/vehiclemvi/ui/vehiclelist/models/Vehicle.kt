package com.talhakosen.vehiclemvi.ui.vehiclelist.models

import com.google.gson.annotations.SerializedName
import com.talhakosen.vehiclemvi.common.database.entities.VehicleEntity
import java.io.Serializable

data class VehicleResponse(
    @SerializedName("count")
    val count: Int = 0,
    @SerializedName("vehicles")
    val vehicles: List<Vehicle> = listOf(),
    @SerializedName("currentPage")
    val currentPage: Int = 0,
    @SerializedName("nextPage")
    val nextPage: Int = 0,
    @SerializedName("totalPages")
    val totalPages: Int = 0
) : Serializable

data class Vehicle(
    @SerializedName("vehicleId")
    val vehicleId: Long,

    @SerializedName("vrn")
    val vrn: String,

    @SerializedName("country")
    val country: String,

    @SerializedName("color")
    val color: Color,

    @SerializedName("type")
    val type: VehicleType,

    @SerializedName("default")
    val default: Boolean = false
) : Serializable

enum class VehicleType(
    val type: String
) {
    @SerializedName("Truck")
    Truck("Truck"),
    @SerializedName("RV")
    RV("RV"),
    @SerializedName("Car")
    Car("Car"),
    @SerializedName("Unknown")
    Unknown("Unknown");

    companion object {
        @JvmStatic
        fun fromType(type: String): VehicleType {
            return when (type) {
                Truck.type -> VehicleType.Truck
                RV.type -> VehicleType.RV
                Car.type -> VehicleType.Car
                else -> VehicleType.Unknown
            }
        }
    }

}

enum class Color(
    val type: String
) {
    @SerializedName("Blue")
    Blue("Blue"),
    @SerializedName("White")
    White("White"),
    @SerializedName("Green")
    Green("Green"),
    @SerializedName("Yellow")
    Yellow("Yellow"),
    @SerializedName("Brown")
    Brown("Brown"),
    @SerializedName("Red")
    Red("Red"),
    @SerializedName("Black")
    Black("Black"),
    @SerializedName("unknown")
    Unknown("unknown");

    companion object {
        @JvmStatic
        fun fromType(type: String): Color {
            return when (type) {
                Blue.type -> Color.Blue
                White.type -> Color.White
                Green.type -> Color.Green
                Yellow.type -> Color.Yellow
                Brown.type -> Color.Brown
                Red.type -> Color.Red
                Black.type -> Color.Black
                else -> Color.Unknown
            }
        }
    }
}


fun Vehicle.toEntity(): VehicleEntity {
    return VehicleEntity(
        vehicleId = vehicleId,
        vrn = vrn,
        country = country,
        color = color.type,
        type = type.type,
        default = default
    )
}


fun VehicleEntity.toModel(): Vehicle {
    return Vehicle(
        vehicleId = vehicleId,
        vrn = vrn,
        country = country,
        color = Color.fromType(color),
        type = VehicleType.fromType(type),
        default = default
    )
}