package com.talhakosen.vehiclemvi.ui.vehicledetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatImageView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.talhakosen.vehiclemvi.R
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.util.bindView
import com.talhakosen.vehiclemvi.util.ext.setColor
import com.talhakosen.vehiclemvi.util.ext.setInvisible
import com.talhakosen.vehiclemvi.util.ext.setVisible

class VehicleDetailFragment : Fragment() {

    private val colorLayout by bindView<FrameLayout>(R.id.vehicle_layout_color)
    private val txtId by bindView<TextView>(R.id.vehicle_layout_id_text)
    private val txtName by bindView<TextView>(R.id.vehicle_layout_name_text)
    private val txtCountry by bindView<TextView>(R.id.vehicle_layout_country_text)
    private val txtType by bindView<TextView>(R.id.vehicle_layout_type_text)
    private val defaultIcon by bindView<AppCompatImageView>(R.id.vehicle_layout_default_icon)

    var vehicle: Vehicle? = null

    companion object {
        private const val SELECTED_VEHICLE = "SELECTED_VEHICLE"

        fun newInstance(vehicle: Vehicle): VehicleDetailFragment {
            val args = Bundle()
            args.putSerializable(SELECTED_VEHICLE, vehicle)

            val fragment = VehicleDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            vehicle = it.getSerializable(SELECTED_VEHICLE) as Vehicle
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vehicle_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vehicle?.let {
            with(it) {
                txtId.text = vehicleId.toString()
                txtCountry.text = country
                txtName.text = vrn
                txtType.text = type.name

                if (default) {
                    defaultIcon.setVisible()
                } else {
                    defaultIcon.setInvisible()
                }
                colorLayout.setColor(color)
            }
        }

    }
}
