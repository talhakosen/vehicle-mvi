package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.repositories

import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.ui.vehiclelist.api.VehicleApi
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.toModel
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class VehicleListRepositories(
    private var api: VehicleApi,
    private var vehiclesDao: VehicleDao
) {
    // Delay added to simulate latency
    fun getVehicles(): Single<VehicleResponse> {
        return api.getVehicles()
            .delay(1000, TimeUnit.MILLISECONDS)
    }

    fun getOfflineVehicles(): Observable<List<Vehicle>> {
        return vehiclesDao
            .getAll()
            .toObservable()
            .map { vehicles -> vehicles.map { vehicleEntity -> vehicleEntity.toModel() } }
    }
}
