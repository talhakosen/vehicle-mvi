package com.talhakosen.vehiclemvi.ui.vehiclelist.adapter

import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.util.DelegatedAdapter
import com.talhakosen.vehiclemvi.util.TypedAdapterDelegate
import com.talhakosen.vehiclemvi.util.ViewHolderRenderer

class VehicleListAdapter(
    private var itemClickListener: OnItemClickListener
) : DelegatedAdapter() {

    companion object {
        private const val TYPE_PRODUCT = 0
    }

    override var items: MutableList<Vehicle> = mutableListOf()

    init {
        addDelegate(TYPE_PRODUCT, TypedAdapterDelegate { parent ->
            val layout = VehicleLayout(parent.context).apply {
                setOnItemClickListener(itemClickListener)
            }
            ViewHolderRenderer(layout)
        })
    }

    override fun getItemViewType(position: Int): Int {
        // Allow you decide other type of layout
        return TYPE_PRODUCT
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateItems(items: List<Vehicle>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}
