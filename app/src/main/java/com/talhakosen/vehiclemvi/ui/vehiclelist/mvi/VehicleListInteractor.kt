package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi

import com.talhakosen.vehiclemvi.base.mvi.MVIInteractor
import com.talhakosen.vehiclemvi.common.NetworkManager
import com.talhakosen.vehiclemvi.common.di.IoScheduler
import com.talhakosen.vehiclemvi.common.navigation.Navigator
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.repositories.VehicleListRepositories
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.usecases.SaveVehiclesUseCase
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import timber.log.Timber
import javax.inject.Inject

class VehicleListInteractor @Inject constructor(
    @IoScheduler private val scheduler: Scheduler,
    private val repositories: VehicleListRepositories,
    private val saveVehicleUseCase: SaveVehiclesUseCase,
    private val networkManager: NetworkManager,
    private val navigator: Navigator
) : MVIInteractor<VehicleListAction, VehicleListResult> {

    private val getVehiclesProcessor =
        ObservableTransformer<VehicleListAction, VehicleListResult> { action ->
            action.switchMap { Observable.just(networkManager.isNetworkAvailable()) }
                .filter { it }
                .flatMapSingle { repositories.getVehicles() }
                .flatMapSingle { saveVehicleUseCase.execute(it.vehicles).toSingleDefault(it) }
                .map { VehicleListResult.Vehicles(it) as VehicleListResult }
                .doOnError { Timber.e(it) }
                .onErrorReturn { error -> VehicleListResult.Error(error.message) }
        }

    private val getOfflineProcessor =
        ObservableTransformer<VehicleListAction, VehicleListResult> { action ->
            action.switchMap { repositories.getOfflineVehicles() }
                .map { VehicleListResult.Vehicles(VehicleResponse(vehicles = it)) as VehicleListResult }
                .doOnNext { Timber.d("") }
                .doOnError { Timber.e(it) }
                .onErrorReturn { error -> VehicleListResult.Error(error.message) }
        }

    private val goToDetailProcessor =
        ObservableTransformer<VehicleListAction.GoToDetail, VehicleListResult> { action ->
            action.flatMapCompletable {
                Completable.fromCallable {
                    navigator.addVehicleDetailFragment(it.vehicle)
                }
            }.toObservable()
        }

    override fun actionProcessor(): ObservableTransformer<in VehicleListAction, out VehicleListResult> =
        ObservableTransformer { actions ->
            actions.observeOn(scheduler)
                .publish { shared ->
                    Observable.merge(
                        listOf(
                            shared.ofType(VehicleListAction.GetLastState::class.java)
                                .map { VehicleListResult.LastState },
                            shared.ofType(VehicleListAction.Init::class.java)
                                .compose(if (networkManager.isNetworkAvailable()) getVehiclesProcessor else getOfflineProcessor),
                            shared.ofType(VehicleListAction.LoadMore::class.java)
                                .compose(getVehiclesProcessor),
                            shared.ofType(VehicleListAction.GoToDetail::class.java)
                                .compose(goToDetailProcessor)
                        )
                    )
                }
        }
}
