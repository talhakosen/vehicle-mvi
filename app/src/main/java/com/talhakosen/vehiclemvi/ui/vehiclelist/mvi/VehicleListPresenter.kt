package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi

import com.talhakosen.vehiclemvi.base.mvi.MVIBasePresenter
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import javax.inject.Inject

class VehicleListPresenter @Inject constructor(
    interactor: VehicleListInteractor
) : MVIBasePresenter<VehicleListIntention, VehicleListState, VehicleListAction, VehicleListResult>(interactor) {

    override val defaultState: VehicleListState
        get() = VehicleListState(
            error = "",
            vehicle = VehicleResponse()
        )

    override val lastStateIntention: VehicleListIntention
        get() = VehicleListIntention.GetLastState

    override fun intentionToActionMapper() =
        { intention: VehicleListIntention ->
            when (intention) {
                VehicleListIntention.GetLastState -> VehicleListAction.GetLastState
                is VehicleListIntention.Init -> VehicleListAction.Init
                is VehicleListIntention.GoToDetail -> VehicleListAction.GoToDetail(intention.vehicle)
                is VehicleListIntention.LoadMore -> VehicleListAction.LoadMore(intention.startPage, intention.endPage)
            }
        }

    override fun stateReducer() =
        { prevState: VehicleListState, result: VehicleListResult ->
            when (result) {
                is VehicleListResult.Vehicles -> {
                    prevState.copy(
                        vehicle = result.vehicle
                    )
                }
                VehicleListResult.LastState -> prevState
                is VehicleListResult.Error -> prevState.copy(
                    vehicle = VehicleResponse(),
                    error = result.error ?: "Unknown Error"
                )
            }
        }
}
