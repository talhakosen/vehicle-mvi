package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.usecases

import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.toEntity
import io.reactivex.Completable
import javax.inject.Inject

class SaveVehiclesUseCase @Inject constructor(
    var dao: VehicleDao
) {
    fun execute(vehicles: List<Vehicle>): Completable {
        return Completable.fromAction {
            dao.insert(vehicles.map { it.toEntity() })
        }
    }
}
