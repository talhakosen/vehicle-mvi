package com.talhakosen.vehiclemvi.ui.vehiclelist.di

import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.ui.vehiclelist.api.VehicleApi
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.repositories.VehicleListRepositories
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit


@Module
object VehicleListModule {

    /**
     * Provides the VehicleList repository implementation.
     * @param api the VehicleApi object used to instantiate the repository
     * @param dao the VehicleDao object used to instantiate the repository
     * @return the VehicleList repository
     */
    @Provides
    @Reusable
    @JvmStatic
    fun provideVehicleListRepositories(api: VehicleApi, dao: VehicleDao) = VehicleListRepositories(api, dao)

    /**
     * Provides the Vehicle service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Vehicle service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideApi(retrofit: Retrofit): VehicleApi {
        return retrofit.create(VehicleApi::class.java)
    }
}
