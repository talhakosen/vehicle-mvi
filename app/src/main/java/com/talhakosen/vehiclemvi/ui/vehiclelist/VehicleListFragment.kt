package com.talhakosen.vehiclemvi.ui.vehiclelist

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.talhakosen.vehiclemvi.R
import com.talhakosen.vehiclemvi.base.mvi.MVIView
import com.talhakosen.vehiclemvi.ui.vehiclelist.adapter.VehicleListAdapter
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.VehicleListIntention
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.VehicleListPresenter
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.VehicleListState
import com.talhakosen.vehiclemvi.util.ItemOffsetDecoration
import com.talhakosen.vehiclemvi.util.bindView
import com.talhakosen.vehiclemvi.util.ext.getDimensionPixelSizeCompat
import com.talhakosen.vehiclemvi.util.ext.setGone
import com.talhakosen.vehiclemvi.util.ext.setVisible
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class VehicleListFragment @Inject constructor() : DaggerFragment(),
    MVIView {

    private val recyclerView by bindView<RecyclerView>(R.id.vehicle_list_recycler_view)
    private val loadingView by bindView<CardView>(R.id.vehicle_list_loading_view)
    private val intentionsSubject = PublishSubject.create<VehicleListIntention>()
    private val compositeDisposable = CompositeDisposable()
    private lateinit var adapter: VehicleListAdapter
    lateinit var currentState: VehicleListState

    companion object {
        private const val VISIBLE_THRESHOLD = 1
        private var CURRENT_PAGE = 1
        private var IS_LOADING = false
    }

    @Inject
    lateinit var presenter: VehicleListPresenter

    override fun onStart() {
        super.onStart()
        compositeDisposable.add(
            presenter.states()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::render)
        )

        compositeDisposable.add(presenter.processIntentions(intentions()))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vehicle_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = VehicleListAdapter { vehicle ->
            intentionsSubject.onNext(
                VehicleListIntention.GoToDetail(vehicle = vehicle)
            )
        }

        addDecorationToRecyclerView()
        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter
        addInfiniteScrollToRecyclerView(linearLayoutManager)
    }

    private fun addInfiniteScrollToRecyclerView(linearLayoutManager: LinearLayoutManager) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!IS_LOADING && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    loadingView.setVisible()
                    CURRENT_PAGE++
                    intentionsSubject.onNext(
                        VehicleListIntention.LoadMore(currentState.vehicle.currentPage, currentState.vehicle.nextPage)
                    )
                    IS_LOADING = true
                }
            }
        })
    }

    private fun addDecorationToRecyclerView() {
        context?.let {
            val leftRightOffset = it.getDimensionPixelSizeCompat(R.dimen.padding_5dp)
            val topOffset = it.getDimensionPixelSizeCompat(R.dimen.padding_5dp)
            val bottomOffset = it.getDimensionPixelSizeCompat(R.dimen.padding_5dp)
            recyclerView.addItemDecoration(
                ItemOffsetDecoration(
                    leftOffset = leftRightOffset,
                    topOffset = topOffset,
                    bottomOffset = bottomOffset,
                    rightOffset = leftRightOffset
                )
            )
        }
    }

    private fun intentions(): Observable<VehicleListIntention> = Observable.merge(
        listOf(
            Observable.just(VehicleListIntention.Init),
            intentionsSubject
        )
    )

    @UiThread
    private fun render(state: VehicleListState) {
        IS_LOADING = false
        loadingView.setGone()
        currentState = state

        if (state.error.isEmpty()) {
            state.vehicle.let {
                adapter.updateItems(state.vehicle.vehicles)
            }
        } else {
            Toast.makeText(context, state.error, Toast.LENGTH_SHORT).show()
        }
    }
}
