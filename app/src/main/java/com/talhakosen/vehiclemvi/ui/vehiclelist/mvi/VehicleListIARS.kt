package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi

import com.talhakosen.vehiclemvi.base.mvi.InitialIntention
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse

// region Intentions
///////////////////////////////////////////////////////////////////////////

sealed class VehicleListIntention {
    object Init : VehicleListIntention(),
        InitialIntention

    object GetLastState : VehicleListIntention()

    data class GoToDetail(val vehicle: Vehicle) : VehicleListIntention()

    data class LoadMore(val startPage: Int, val endPage: Int) : VehicleListIntention()
}

// endregion

// region Actions
///////////////////////////////////////////////////////////////////////////

sealed class VehicleListAction {
    object Init : VehicleListAction()

    object GetLastState : VehicleListAction()

    data class GoToDetail(val vehicle: Vehicle) : VehicleListAction()

    data class LoadMore(val startPage: Int, val endPage: Int) : VehicleListAction()
}

// endregion

// region Result
///////////////////////////////////////////////////////////////////////////

sealed class VehicleListResult {

    data class Vehicles(val vehicle: VehicleResponse) : VehicleListResult()

    data class Error(val error: String?) : VehicleListResult()

    object LastState : VehicleListResult()
}

// endregion

// region CreateUserState
///////////////////////////////////////////////////////////////////////////

data class VehicleListState(
    val error: String = "",
    val vehicle: VehicleResponse = VehicleResponse()
)

// endregion


