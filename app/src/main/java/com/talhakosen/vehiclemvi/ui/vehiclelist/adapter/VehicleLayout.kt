package com.talhakosen.vehiclemvi.ui.vehiclelist.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import com.talhakosen.vehiclemvi.R
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.util.ItemRenderer
import com.talhakosen.vehiclemvi.util.bindView
import com.talhakosen.vehiclemvi.util.ext.applyLayoutParams
import com.talhakosen.vehiclemvi.util.ext.doInRuntime
import com.talhakosen.vehiclemvi.util.ext.selfInflate

class VehicleLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr),
    ItemRenderer<Vehicle> {

    private val vehicleName by bindView<TextView>(R.id.vehicle_layout_name_text)
    private var onItemClickListener: OnItemClickListener? = null
    private var model: Vehicle? = null

    init {
        selfInflate(R.layout.layout_vehicle_item)

        doInRuntime {
            applyLayoutParams(MATCH_PARENT, WRAP_CONTENT)
            cardElevation = 0f
            setContentPadding(0, 0, 0, 0)
            setOnClickListener {
                model?.let { model ->
                    onItemClickListener?.invoke(model)
                }
            }
        }
    }

    override fun render(data: Vehicle) {
        model = data
        vehicleName.text = data.vrn
    }

    fun setOnItemClickListener(clickListener: OnItemClickListener?) {
        this.onItemClickListener = clickListener
    }
}

// region Listeners
///////////////////////////////////////////////////////////////////////////

typealias OnItemClickListener = (vehicle: Vehicle) -> Unit

///////////////////////////////////////////////////////////////////////////
