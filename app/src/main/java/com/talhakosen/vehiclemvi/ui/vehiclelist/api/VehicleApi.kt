package com.talhakosen.vehiclemvi.ui.vehiclelist.api

import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import io.reactivex.Single
import retrofit2.http.GET

interface VehicleApi {

    @GET("/vehicles")
    fun getVehicles(): Single<VehicleResponse>
}
