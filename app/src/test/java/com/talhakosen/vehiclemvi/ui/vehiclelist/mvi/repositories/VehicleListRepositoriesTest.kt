package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.repositories

import com.nhaarman.mockitokotlin2.whenever
import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.ui.vehiclelist.api.VehicleApi
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.toEntity
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.dummyResponse
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.util.concurrent.TimeUnit

class VehicleListRepositoriesTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var vehicleDao: VehicleDao

    @Mock
    private lateinit var vehicleApi: VehicleApi

    private lateinit var repository: VehicleListRepositories

    private val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        repository = VehicleListRepositories(api = vehicleApi, vehiclesDao = vehicleDao)
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
    }

    @Test
    fun `api should return correct response with delay`() {
        whenever(vehicleApi.getVehicles()).thenReturn(Single.just(dummyResponse))

        val testObserver = repository.getVehicles().test()

        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        testObserver.assertValues(dummyResponse)
    }

    @Test
    fun `database should return correct response`() {
        whenever(vehicleDao.getAll()).thenReturn(Flowable.just(dummyResponse.vehicles.map { it.toEntity() }))

        val testObserver = repository.getOfflineVehicles().test()

        testObserver.assertValues(dummyResponse.vehicles)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
    }
}
