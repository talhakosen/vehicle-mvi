package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.usecases

import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.toEntity
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.dummyResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class SaveVehiclesUseCaseTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var vehicleDao: VehicleDao

    private lateinit var saveVehiclesUseCase: SaveVehiclesUseCase

    @Before
    fun setUp() {
        saveVehiclesUseCase = SaveVehiclesUseCase(vehicleDao)
    }

    @Test
    fun `when execute called should inset vehicles to database`() {
        val vehicleEntities = dummyResponse.vehicles.map { it.toEntity() }

        val testObserver = saveVehiclesUseCase.execute(dummyResponse.vehicles).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()

        verify(vehicleDao, times(1)).insert(vehicleEntities)
    }
}
