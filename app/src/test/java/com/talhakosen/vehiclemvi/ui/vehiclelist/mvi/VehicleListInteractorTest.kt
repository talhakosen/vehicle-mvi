package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi

import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.talhakosen.vehiclemvi.common.NetworkManager
import com.talhakosen.vehiclemvi.common.database.VehicleDao
import com.talhakosen.vehiclemvi.common.navigation.Navigator
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.repositories.VehicleListRepositories
import com.talhakosen.vehiclemvi.ui.vehiclelist.mvi.usecases.SaveVehiclesUseCase
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.*
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class VehicleListInteractorTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private lateinit var disposables: CompositeDisposable
    private val schedulerTrampoline = Schedulers.trampoline()

    @Mock
    private lateinit var navigator: Navigator

    @Mock
    private lateinit var networkManager: NetworkManager

    @Mock
    private lateinit var vehicleListRepositories: VehicleListRepositories

    @Mock
    private lateinit var saveVehicleUseCase: SaveVehiclesUseCase

    @Mock
    private lateinit var vehicleDao: VehicleDao

    private val actual = Completable.complete()

    @Before
    fun setUp() {
        disposables = CompositeDisposable()
    }

    @After
    fun tearDown() {
        disposables.dispose()
    }

    // region Test online

    @Test
    fun `when init action with internet connection should return correct result`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first()
        Assert.assertTrue(result == VehicleListResult.Vehicles(dummyResponse))
    }

    @Test
    fun `when LoadMore action with internet connection should return correct result`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first()
        Assert.assertTrue(result == VehicleListResult.Vehicles(dummyResponse))
    }

    @Test
    fun `when init action with internet connection should contains correct vehicle`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first() as VehicleListResult.Vehicles
        assertThat(result.vehicle.vehicles, CoreMatchers.hasItem(dummyVehicleOne))
    }

    @Test
    fun `when init action with internet connection should return correct list`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first() as VehicleListResult.Vehicles
        assertThat(result.vehicle.vehicles.size, CoreMatchers.`is`(3))
    }

    @Test
    fun `when init action with internet connection should save vehicles to database`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        verify(saveVehicleUseCase, times(1)).execute(dummyResponse.vehicles)
    }

    @Test
    fun `when init action with internet connection should not query database`() {
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.just(dummyResponse))
        whenever(saveVehicleUseCase.execute(dummyResponse.vehicles)).thenReturn(actual)

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        verify(vehicleListRepositories, never()).getOfflineVehicles()
    }

    @Test
    fun `when init action with internet connection has error should return correct result`() {
        val throwable = Throwable()
        whenever(networkManager.isNetworkAvailable()).thenReturn(true)
        whenever(vehicleListRepositories.getVehicles()).thenReturn(Single.error(throwable))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first()
        Assert.assertTrue(result == VehicleListResult.Error(throwable.message))
    }

    // endregion

    // region  Test Offline

    @Test
    fun `when init action without internet connection should return correct result`() {
        val vehicles = dummyResponse.vehicles
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.just(vehicles))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first()
        Assert.assertTrue(result == VehicleListResult.Vehicles(VehicleResponse(vehicles = vehicles)))
    }

    @Test
    fun `when init action without internet connection should contains correct vehicle`() {
        val vehicles = dummyResponse.vehicles
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.just(vehicles))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first() as VehicleListResult.Vehicles
        assertThat(result.vehicle.vehicles, CoreMatchers.hasItem(dummyVehicleOne))
    }

    @Test
    fun `when init action without internet connection should return correct list`() {
        val vehicles = dummyResponse.vehicles
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.just(vehicles))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first() as VehicleListResult.Vehicles
        assertThat(result.vehicle.vehicles.size, CoreMatchers.`is`(3))
    }

    @Test
    fun `when init action without internet connection should not save vehicles to database`() {
        val vehicles = dummyResponse.vehicles
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.just(vehicles))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        verify(saveVehicleUseCase, never()).execute(dummyResponse.vehicles)
    }

    @Test
    fun `when init action without internet connection should get vehicles from database`() {
        val vehicles = dummyResponse.vehicles
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.just(vehicles))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        verify(vehicleListRepositories, never()).getVehicles()
    }

    @Test
    fun `when init action withoutinternet connection has error should return correct result`() {
        val throwable = Throwable()
        whenever(networkManager.isNetworkAvailable()).thenReturn(false)
        whenever(vehicleListRepositories.getOfflineVehicles()).thenReturn(Observable.error(throwable))

        val testObserver = createResultObservable(VehicleListAction.Init).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        val result = testObserver.events[0].first()
        Assert.assertTrue(result == VehicleListResult.Error(throwable.message))
    }

    // endregion

    // region  Test Offline

    @Test
    fun `when GoToDetail action should go correct destination with correct data`() {
        val testObserver = createResultObservable(VehicleListAction.GoToDetail(dummyVehicleOne)).test().apply {
            disposables.add(this)
        }

        testObserver.assertNoErrors()

        verify(navigator).addVehicleDetailFragment(dummyVehicleOne)
    }

    // endregion

    // region Test Helpers

    private fun createResultObservable(
        action: VehicleListAction,
        scheduler: Scheduler = schedulerTrampoline
    ) = Observable.fromCallable { action }.compose(createInteractor(scheduler).actionProcessor())

    private fun createInteractor(scheduler: Scheduler = schedulerTrampoline) =
        VehicleListInteractor(
            scheduler = scheduler,
            navigator = navigator,
            networkManager = networkManager,
            repositories = vehicleListRepositories,
            saveVehicleUseCase = saveVehicleUseCase
        )
    // endregion
}
