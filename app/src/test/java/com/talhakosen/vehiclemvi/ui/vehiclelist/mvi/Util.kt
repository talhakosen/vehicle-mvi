package com.talhakosen.vehiclemvi.ui.vehiclelist.mvi

import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Color
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.Vehicle
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleResponse
import com.talhakosen.vehiclemvi.ui.vehiclelist.models.VehicleType

// region Test Helpers

val dummyVehicleOne = Vehicle(
    vehicleId = 1,
    vrn = "vrn1",
    type = VehicleType.RV,
    default = true,
    color = Color.Black,
    country = "ES"
)

val dummyVehicleTwo = Vehicle(
    vehicleId = 2,
    vrn = "vrn2",
    type = VehicleType.Car,
    default = false,
    color = Color.Brown,
    country = "NL"
)

val dummyVehicleThree = Vehicle(
    vehicleId = 3,
    vrn = "vrn3",
    type = VehicleType.Truck,
    default = false,
    color = Color.Green,
    country = "FR"
)

val dummyResponse = VehicleResponse(
    count = 5,
    vehicles = listOf(
        dummyVehicleOne,
        dummyVehicleTwo,
        dummyVehicleThree
    ),
    currentPage = 0,
    nextPage = 0,
    totalPages = 1
)

// endregion